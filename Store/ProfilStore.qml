pragma Singleton
import QtQuick 2.0

import "../Actions"
import "."

Item {
    property variant profil : BDD.profils;
    Connections {
        target: AppDispatcher;
        onDispatched: {
            if(action === ActionType.createProfil)
                BDD.createProfil(args.nom, args.prenom);
        }
    }
}

