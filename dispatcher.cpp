#include "dispatcher.h"

Dispatcher::Dispatcher(QObject *parent) : QObject(parent)
{

}

void Dispatcher::dispatch(QString action, QJSValue args) {
    emit dispatched(action, args);
}
