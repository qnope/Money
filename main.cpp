#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>
#include "dispatcher.h"
#include "bddconnect.h"
#include "Table/profiltable.h"
#include "Table/achattable.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    Dispatcher dispatcher;

    BDDConnect connection;

    ProfilTable profilTable(connection);
    AchatTable achatTable(connection);

    engine.rootContext()->setContextProperty("profilModel", &profilTable);
    engine.rootContext()->setContextProperty("achatModel", &achatTable);
    engine.rootContext()->setContextProperty("AppDispatcher", &dispatcher);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

