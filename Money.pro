TEMPLATE = app

QT += qml quick widgets sql

SOURCES += main.cpp \
    dispatcher.cpp \
    bddconnect.cpp \
    Table/profiltable.cpp \
    Table/achattable.cpp \
    Table/abstractqmltable.cpp

RESOURCES += qml.qrc

CONFIG += c++14

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

HEADERS += \
    dispatcher.h \
    bddconnect.h \
    Table/abstractqmltable.h \
    Table/profiltable.h \
    Table/achattable.h

DISTFILES +=


