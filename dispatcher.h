#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <QObject>
#include <QJSValue>

class Dispatcher : public QObject
{
    Q_OBJECT
public:
    explicit Dispatcher(QObject *parent = 0);

signals:
    void dispatched(QString action, QJSValue args);

public slots:
    Q_INVOKABLE void dispatch(QString action, QJSValue args);
};

#endif // DISPATCHER_H
