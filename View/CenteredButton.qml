import QtQuick 2.0
import QtQuick.Controls 1.4

Button {
    anchors.horizontalCenter: parent.horizontalCenter;
    anchors.verticalCenter: parent.verticalCenter;

    width: parent.width / 5;
    height: parent.height / 10;
}
