import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Window 2.0

import "../Actions"

Window {
    SystemPalette {
        id: palette;
    }

    visible: false;
    width: 480;
    height: 320;
    color: palette.window;

    property alias prenom: prenomField.text;
    property alias nom: nomField.text;

    CenteredTextField {
        id: nomField;
        placeholderText: "Nom";
        anchors.verticalCenterOffset: -parent.height / 10;
    }

    CenteredTextField {
        id: prenomField;
        placeholderText: "Prenom";
    }

    CenteredButton {
        onClicked: {
            AppAction.createProfil(prenom, nom);
            close();
        }

        anchors.horizontalCenterOffset: parent.width / 8;
        anchors.verticalCenterOffset: parent.height / 10;
        text: "Ok";
    }

    CenteredButton {
        onClicked: {
            close();
        }

        anchors.horizontalCenterOffset: -parent.width / 8;
        anchors.verticalCenterOffset: parent.height / 10;
        text: "Annuler";
    }
}

