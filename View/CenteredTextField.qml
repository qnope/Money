import QtQuick 2.0
import QtQuick.Controls 1.4

TextField {
    anchors.horizontalCenter: parent.horizontalCenter;
    anchors.verticalCenter: parent.verticalCenter;

    width: parent.width / 3;
    height: parent.height / 15;
}

