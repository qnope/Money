import QtQuick 2.5
import QtQuick.Controls 1.4

import "../Actions"

MenuBar {
    Menu {
        title: "Fichier";

        Menu {
            title: "Nouveau";

            MenuItem {
                text: "Profil";
                iconSource: "qrc:/Icon/newProfil.jpg";
                onTriggered: AppAction.askToCreateProfil();
            }

            MenuItem {
                text: "Dépense";
                iconSource: "qrc:/Icon/newDepense.png";
            }
        }

        MenuItem {
            text: "Sauvegarder";
            iconSource: "qrc:/Icon/save.png";
        }

        MenuItem {
            text: "Quitter";
            iconSource: "qrc:/Icon/exit.png";
            onTriggered: AppAction.exit();
        }
    }

    Menu {
        title: "Éditer";

        Menu {
            title: "Modifier";

            MenuItem {
                text: "Profil";
                iconSource: "qrc:/Icon/modifierProfil.jpg";
            }

            MenuItem {
                text: "Dépense";
                iconSource: "qrc:/Icon/modifierDepense.jpg";
            }
        }

        Menu {
            title: "Supprimer";

            MenuItem {
                text: "Profil";
                iconSource: "qrc:/Icon/deleteProfil.jpg";
            }

            MenuItem {
                text: "Dépense";
                iconSource: "qrc:/Icon/deleteMoney.jpg";
            }
        }
    }

    Menu {
        title: "Aide";

        MenuItem {
            text: "À propos";
            iconSource: "qrc:/Icon/about.jpg";
        }
    }
}

