#include "bddconnect.h"
#include <QDebug>
#include <QSqlError>
#include <QSqlRecord>
#include <utility>
#include <QSqlField>

using namespace std;

BDDConnect::BDDConnect() : QSqlDatabase(QSqlDatabase::addDatabase("QMYSQL")) {
    setHostName("localhost");
    setDatabaseName("Money");
    setUserName("root");
    setPassword("azerty");
    bool ok = open();

    qDebug() << lastError();
    Q_ASSERT(ok);
}
