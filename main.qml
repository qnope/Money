import QtQuick 2.5
import QtQuick.Controls 1.4

import "./Actions"
import "./View"
import "./Store"

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World");

    menuBar: MenuBar {}

    CreateProfilView {
        id: createProfilView;
    }

    Connections {
        target: AppDispatcher;
        onDispatched: {
            if(action === ActionType.exit)
                Qt.quit();

            if(action === ActionType.askToCreateProfil) {
                createProfilView.visible = true;
                createProfilView.nom = "";
                createProfilView.prenom = "";
            }
        }
    }

    TableView {
        TableViewColumn {
            role: "ID";
            title: "ID";
            width: 100;
        }

        TableViewColumn {
            role: "Nom";
            title: "Nom";
            width: 100;
        }

        TableViewColumn {
            role: "Profil_Nom_2";
            title: "Acheteur";
            width: 100;
        }

        TableViewColumn {
            role: "Valeur";
            title: "Valeur";
            width: 100;
        }
        model: achatModel;
    }
}

