#include "achattable.h"
#include <QSqlError>

AchatTable::AchatTable(QSqlDatabase const &connection) : AbstractQMLTable(connection)
{
    setTable("Achat");
    setRelation(2, QSqlRelation("Profil", "ID", "Nom"));
    select();
}

QHash<int, QByteArray> AchatTable::roleNames() const {
    enum ProfilRoles {
        IDRole = Qt::UserRole + 1,
        NOMRole, ACHETEURRole, VALEURRole
    };

    QHash<int, QByteArray> roles;
    roles[IDRole] = "ID";
    roles[NOMRole] = "Nom";
    roles[ACHETEURRole] = "Profil_Nom_2";
    roles[VALEURRole] = "Valeur";
    return roles;
}
