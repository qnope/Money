#include "profiltable.h"

ProfilTable::ProfilTable(QSqlDatabase const &connection) : AbstractQMLTable(connection) {
    setTable("Profil");
    select();
}

QHash<int, QByteArray> ProfilTable::roleNames() const {
    enum ProfilRoles {
        IDRole = Qt::UserRole + 1,
        NOMRole, PRENOMRole
    };

    QHash<int, QByteArray> roles;
    roles[IDRole] = "ID";
    roles[NOMRole] = "Nom";
    roles[PRENOMRole] = "Prenom";
    return roles;
}
