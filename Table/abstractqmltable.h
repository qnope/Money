#ifndef ABSTRACTQMLTABLE_H
#define ABSTRACTQMLTABLE_H

#include <QSqlRelationalDelegate>
#include <QSqlRecord>
#include <QDebug>

class AbstractQMLTable : public QSqlRelationalTableModel
{
    Q_OBJECT
protected:
    AbstractQMLTable(QSqlDatabase const &connection);

public:
    Q_INVOKABLE QVariant data(const QModelIndex &item, int role = Qt::DisplayRole) const override;
};

#endif // ABSTRACTQMLTABLE_H
