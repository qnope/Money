#ifndef ACHATTABLE_H
#define ACHATTABLE_H

#include "abstractqmltable.h"
#include <QHash>

class AchatTable : public AbstractQMLTable
{
    Q_OBJECT
public:
    AchatTable(QSqlDatabase const &connection);

    QHash<int, QByteArray> roleNames() const override;
};

#endif // ACHATTABLE_H
