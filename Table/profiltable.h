#ifndef PROFILTABLE_H
#define PROFILTABLE_H

#include "abstractqmltable.h"
#include <QHash>

class ProfilTable : public AbstractQMLTable
{
    Q_OBJECT
public:
    ProfilTable(QSqlDatabase const &connection);

    QHash<int, QByteArray> roleNames() const override;
};

#endif // PROFILTABLE_H
