#include "abstractqmltable.h"

AbstractQMLTable::AbstractQMLTable(const QSqlDatabase &connection) :
    QSqlRelationalTableModel(nullptr, connection) {
    //setEditStrategy(OnFieldChange);
}

QVariant AbstractQMLTable::data(const QModelIndex &item, int role) const {
    if(role > Qt::UserRole)
        return record(item.row()).value(QString(roleNames().value(role)));
    return QSqlRelationalTableModel::data(item, role);
}
