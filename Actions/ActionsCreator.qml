pragma Singleton
import QtQuick 2.0

import "."

QtObject {
    function exit() {
        AppDispatcher.dispatch(ActionType.exit, undefined);
    }

    function askToCreateProfil() {
        AppDispatcher.dispatch(ActionType.askToCreateProfil, undefined);
    }

    function askToCreateDepense() {
        AppDispatcher.dispatch(ActionType.askToCreateDepense, undefined);
    }

    function askToModifyProfil() {
        AppDispatcher.dispatch(ActionType.askToModifyProfil, undefined);
    }

    function askToRemoveProfil() {
        AppDispatcher.dispatch(ActionType.askToRemoveProfil, undefined);
    }

    function askToRemoveDepense() {
        AppDispatcher.dispatch(ActionType.askToRemoveDepense, undefined);
    }

    function createProfil(prenom, nom) {
        AppDispatcher.dispatch(ActionType.createProfil,
                               {prenom: prenom, nom: nom});
    }
}

