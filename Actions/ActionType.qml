pragma Singleton
import QtQuick 2.0

QtObject {
    property string exit: "Exit";
    property string askToCreateProfil: "AskToCreateProfil";
    property string askToCreateDepense: "AskToCreateDepense";
    property string askToModifyProfil: "AskToModifyProfil";
    property string askToModifyDepense: "AskToModifyDeoense";
    property string askToRemoveProfil: "AskToRemoveProfil";
    property string askToRemoveDepense: "AskToRemoveDepense";
    property string createProfil: "CreateProfil";
    property string createDepense: "CreateDepense";
    property string modifyProfil: "ModifyProfil";
    property string modifyDepense: "ModifyDepense";
    property string removeProfil: "RemoveProfil";
    property string removeDepense: "RemoveDepense";
}

